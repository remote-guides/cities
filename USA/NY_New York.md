# New York City, NY, USA

## Working spaces

- [Spacious](https://www.spacious.com/) ($20 per day) 
  - Quiet: [Hearth](https://www.spacious.com/spaces/hearth)
  - Quiet: [Aldea](https://www.spacious.com/spaces/aldea)
- [Croissant](https://www.getcroissant.com/discover/new-york)
- [Kettlespace](https://www.kettlespace.com/locations)

## Neighbourhood: Manhattan

### What to eat

- 🍕 Pizza
  - Joe’s Pizza – West Village or Union Square
- 🍔 Burgers
  - Shake Shack in Madison Square Park (views of Empire State Building and Flatiron Building)
  - [The Happiest Hour](https://www.happiesthournyc.com/) – Greenwich Village, imagine what the best Big Mac could be. 
- 🍳 Breakfast / brunch
  - [Citizens of Chelsea](https://citizens.coffee/) - does not take reservations
  - [Lafayette Grand Café & Bakery](http://lafayetteny.com/)
  - [Buvette](https://ilovebuvette.com/)
- 🐟 [Russ and Daughters](https://www.russanddaughters.com/)
- 🍜 Ramen
  - [Tonchin](https://www.tonchinnewyork.com/) – midtown ramen and cocktails. Better than Ippudo…
  - Ippudo – multiple locations
- 🥩 Peter Lugers - Fancy Steakhouse if you’re looking for a nice steak dinner out 
  - They only take cash.  Reservations are difficult to obtain.
- 🍮 [Rice to Riches](https://www.ricetoriches.com/) - rice pudding - near the Bowery!
- 🍹 [1803 NYC](https://www.1803nyc.com/) - Cajun restaurant with a nice happy hour
- 🥪 [Katz’s](https://www.katzsdelicatessen.com/) - Classic Jewish deli on the lower east side
- 🍖 [Gaonnuri](https://www.gaonnurinyc.com/) - Upscale Korean BBQ with a nice view
- 🥐 Pastries, bread and sweets
  - [Chanson](https://www.patisseriechanson.com/) (Madison Square Park) – for kouign-amann
  - [Breads Bakery](https://www.breadsbakery.com/) (Union Square) – it’s all about the babka
  - The Donut Pub (14th St) – casual cronuts

### Where to go

- [The High Line](https://www.thehighline.org/visit/) - Former elevated railroad that is now a public park
- Walk over Brooklyn Bridge
- Brooklyn Bridge Park/Promenade
- [Grant’s Tomb](https://www.nps.gov/gegr/index.htm) - Ever wonder who was buried here? Visit and find out!
- [Staten Island Ferry](http://www.nyc.gov/html/dot/html/ferrybus/staten-island-ferry.shtml) - free - allow at least 90 minutes for a round-trip.
- [Governor’s Island](https://govisland.com/)

## Neighbourhood: Brooklyn

### Where to eat

- Birds of a feather
- 🍕 Pizza
  - Robertas
  - Archies
  - Juliannas
  - Grimalidi’s
  - Sam’s (238 Court St)
- [Pies & Thighs](http://piesnthighs.com/) - fried chicken - when you can’t get into Peter Luger’s :-)
- [Randolph Beer](https://www.randolphbeer.com/) - huge selection of on-draft beer & creative food - two locations.

### Where to go

- Domino Park (new; at Williamsburg waterfront with view of Manhattan)
- Prospect Park (2/3 the size and 1/3 the crowd vs Central Park :) )

## Neighbourhood: Queens

### Where to go

- [Unispehre](https://en.wikipedia.org/wiki/Unisphere) - In the middle of Flushing Meadow park. Better in the summer
  - Near the Queens Museum
- Queens Museum [panorama of NYC](https://queensmuseum.org/2013/10/panorama-of-the-city-of-new-york) - built at the time of the 1964 World’s Fair
  - Near the Unisphere
- ✨ 🗿 Naguchi Museum – in Astoria - beautiful sculptures, quiet and zen.

### Where to eat

- [Kababish](https://www.yelp.com/biz/kababish-jackson-heights) - in Jackson Heights -- it’s where the taxi drivers eat.
- Flushing Chinatown

## Neighborhood: New Jersey

### Where to go

- Liberty State Park - Great views of lower Manhattan and the Statue of Liberty
- [Edison Historic Site](https://www.nps.gov/edis/index.htm) - Lots of history

### Where to eat

- [Brasilia Grill](https://www.brasiliagrill.net/) - Ironbound, Newark - Brasilian / Portuguese food
